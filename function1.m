function output = function1(x)
    output = (5*tan(x.^3)-cos(x))/sin(2.*x);
end