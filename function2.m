function output = function2(x)
    output = (sin(x)-2.*cot(x))/tan(2.*x.^2);
end