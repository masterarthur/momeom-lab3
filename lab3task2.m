a11 = -1.2;
a12 = 0.3;
a13 = -0.2;
a21 = 0.5;
a22 = 2.1;
a23 = 1.3;
a31 = -0.9;
a32 = 0.7;
a33 = 5.6;

d1 = 1.32;
d2 = 3.91;
d3 = 5.4;

C = [a11 a12 a13; a21 a22 a23; a31 a32 a33];
d = [d1; d2; d3];

syms x y z;
equation1 = a11*x+a12*y+a13*z == d1;
equation2 = a21*x+a22*y+a23*z == d2;
equation3 = a31*x+a32*y+a33*z == d3;

solution = solve([equation1, equation2, equation3], [x,y,z]);

disp('x=');
disp(solution.x);
disp('y=');
disp(solution.y);
disp('z=');
disp(solution.z);