a11 = -1.2;
a12 = 0.3;
a13 = -0.2;
a21 = 0.5;
a22 = 2.1;
a23 = 1.3;
a31 = -0.9;
a32 = 0.7;
a33 = 5.6;

d1 = 1.32;
d2 = 3.91;
d3 = 5.4;

A = [a11 a12; a21 a22];
b = [d1; d2];

disp('A=');
disp(A);

disp('b=');
disp(b);

X=A\b;

disp('X=A\b=');
disp(X);
disp('A*X=');
disp(A*X);

x = -3:0.1:3;
x1 = (d1 - a12*x)/a11;
x2 = (d2 - a22*x)/a21;

figure(1)
plot(x,x1,x,x2)
grid on;

A1=A;
A2=A;
A1(1:2) = b;
A2(3:4) = b;

X1 = det(A1)/det(A);
X2 = det(A2)/det(A);

disp('X1=');
disp(X1);
disp('X2=');
disp(X2);
