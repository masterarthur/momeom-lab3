
fplot(@(x) function1(x));
hold on;
fplot(@(x) function2(x));
hold off;

root1 = fzero(@function1, 0.1);
root2 = fzero(@function2, 0.1);

disp('First root of function1: ');
disp(root1);

disp('First root of function2: ');
disp(root2);